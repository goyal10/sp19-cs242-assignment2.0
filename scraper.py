'''
Class to scrape Wikipedia pages for actors and movies and store information
'''

from bs4 import BeautifulSoup
import requests
import logging
import time
import json
from random import randint

from movieScraper import scrape_movie_page
from actorScraper import scrape_actor_page
import utilities as utilities


class Scraper:
	# scrape page at given url
	def scrape_page(self, url):

		# configure_logs()
		# configure_time()

		logging.info("Scraper started")

		actors = []
		movies = []
		actors_urls_queue = []
		movies_urls_queue = []

		movie_page_max = 10
		actor_page_max = 10
		movie_page_scraped = 0
		actor_page_scraped = 0

		base_url = "https://en.wikipedia.org"
		wiki_additive = '/wiki'
		if url is None:
			logging.warning("No url provided. Starting with Morgan Freeman")
			url = base_url + wiki_additive + "/" + "Morgan Freeman"

		first_actor = scrape_actor_page(url)
		actors.append(first_actor)
		actors_urls_queue.append(url)
		actor_page_scraped += 1

		while actor_page_scraped <= actor_page_max or movie_page_scraped <= movie_page_max:
			if len(actors_urls_queue) == 0 and len(movies_urls_queue) == 0:
				logging.warning("nothing left to parse")
				break
			else:
				while len(movies_urls_queue) != 0 and actor_page_scraped <= actor_page_max:
					movie = scrape_movie_page(movies_urls_queue.pop(0))
					actor_page_scraped += self.scrape_new_actors(actors, actors_urls_queue, movie["cast"])

				while len(actors_urls_queue) != 0 and movie_page_scraped <= movie_page_max:
					actor = scrape_actor_page(actors_urls_queue.pop(0))
					movie_page_scraped += self.scrape_new_movies(movies, movies_urls_queue, actor["films"])

		utilities.write_json_in_file("actors_data", {"actors": actors})
		utilities.write_json_in_file("movies_data", {"movies": movies})
		# print(movies)
		# print(actors)

		return [actor_page_scraped, movie_page_scraped]

	def configure_time():
		return

	def configure_logs():
		logging.basicConfig(filename='scraper.log', level=logging.INFO) # 'filemode='w' for fresh file'

	# def scrape_actor(name):
	# 	logging.info("Scraping actor : " + name)

	# 	movies_list = []
	# 	movies_url_list = []

	# 	base_url = "https://en.wikipedia.org"
	# 	wiki_additive = '/wiki'
	# 	url = base_url + wiki_additive + "/" + name

	# 	logging.info("Scraping page now")
	# 	page = requests.get(url)
	# 	soup = BeautifulSoup(page.text, features="html.parser")
	# 	filmography_container = soup.find("span", attrs={"id": "Filmography"})
	# 	# print(filmography_container)

	# 	note_container = filmography_container.find_next()
	# 	# print(note_container)
	# 	notes_url_container = note_container.find_next()
	# 	films_container = notes_url_container.find_next()

	# 	# print(films_container.ul.li.find_next_siblings("li")) # li.a.find_next()
	# 	logging.info(films_container)

	# 	if films_container.ul is not None and films_container.ul.li is not None:
	# 		movies_list.append(films_container.ul.li.a.text)
	# 		films_li_list = films_container.ul.li.find_next_siblings("li")
	# 		# print(films_li_list)

	# 		for elem in films_li_list:
	# 			movies_list.append(elem.a.text)
	# 			temp_url = base_url + elem.a['href']
	# 			movies_url_list.append(temp_url)

	# 		# li_container = films_container.ul.li.a

	# 		logging.info("scraping done")

	# 	else:
	# 		logging.warning("Unable to scrape page for actor: " + name)

	# 	return movies_list, movies_url_list

	# def scrape_movie(name, url=""):
	# 	print(name)
	# 	logging.info("scraping movie: " + name)

	# 	if len(url) == 0:
	# 		base_url = "https://en.wikipedia.org"
	# 		wiki_additive = '/wiki'
	# 		url = base_url + wiki_additive + "/" + name

	# 	actors_list = []
	# 	actors_url_list = []

	# 	logging.info("Scraping page now")
	# 	page = requests.get(url)
	# 	soup = BeautifulSoup(page.text, features="html.parser")
	# 	infobox_container = soup.find("table", attrs={"class": "infobox vevent"})
	# 	table_tags = infobox_container.tbody.tr.find_next_siblings("tr")
		
	# 	for i in range(1, len(table_tags)):
	# 		heading = table_tags[i].th.text
	# 		if heading == "Starring":
	# 			actors_container = table_tags[i].td.find_all("a")
	# 			for a in actors_container:
	# 				actors_list.append(a.text)
	# 				actors_url_list.append(a['href'])
	# 		if heading == "Box office":
	# 			box_office_value = table_tags[i].td.text

	# 	# box_office_value = infobox_container.tbody.tr.find_next_siblings("tr")[-1].td.text

	# 	logging.info("scraping done")

	# 	return box_office_value, actors_list, actors_url_list

	# scrape actors from film data
	def scrape_new_actors(self, actors, actors_urls_queue, actors_from_film):
		count = 0
		for a in actors_from_film:
			if any(a[0] == i["name"] for i in actors):
				continue
			scraped_data = scrape_actor_page(a[1])
			time.sleep(randint(1, 2))
			if scraped_data is None:
				continue
			actors.append(scraped_data)
			actors_urls_queue.append(scraped_data["url"])
			# print(scraped_data)
			count += 1
		return count

	# scrape filmography from actor data
	def scrape_new_movies(self, movies, movies_urls_queue, movies_from_actor):
		count = 0
		for m in movies_from_actor:
			if any(m[0] == i["name"] for i in movies):
				continue
			scraped_data = scrape_movie_page(m[1])
			time.sleep(randint(1, 2))
			if scraped_data is None:
				continue
			movies.append(scraped_data)
			movies_urls_queue.append(scraped_data["url"])
			# print(scraped_data)
			count += 1
		return count

	







		# actors = [actor]
		# actors_url = [wiki_additive + "/" + actor]
		# movies = []
		# movies_url = []
		# box_office_values = []



		# # temp_movies_list, temp_movies_url_list = scrape_actor(actor)

		# # print(temp_movies_list)
		# # print(temp_movies_url_list)

		# movie_page_scraped = 0
		# actor_page_scraped = 0

		# while(len(actors) < actor_page_max and len(movies) < movie_page_max):
		# 	if len(actors) < actor_page_max:
		# 		temp_movies_list, temp_movies_url_list = scrape_actor(actors[actor_page_scraped])
		# 		movies.extend(temp_movies_list)
		# 		print(movies)
		# 		movies_url.extend(temp_movies_url_list)
		# 		actor_page_scraped += 1
		# 	if len(movies) < movie_page_max:
		# 		temp_box_office_value, temp_actors_list, temp_actors_url_list = scrape_movie(movies[movie_page_scraped]) #  base_url + movies_url[movie_page_scraped]
		# 		box_office_values.extend(temp_box_office_value)
		# 		actors.extend(temp_actors_list)
		# 		actors_url.extend(temp_actors_url_list)
		# 		actor_page_scraped += 1

		# 	time.sleep(randint(1, 2))




	#while (false):
		

	# page = requests.get(url)
	# soup = BeautifulSoup(page.text, features="html.parser")
	# filmography_container = soup.find("span", attrs={"id": "Filmography"})
	# # print(filmography_container)

	# note_container = filmography_container.find_next()
	# # print(note_container)
	# notes_url_container = note_container.find_next()
	# films_container = notes_url_container.find_next()

	# # print(films_container.ul.li.find_next_siblings("li")) # li.a.find_next()

	# final_list = films_container.ul.li.find_next_siblings("li")
	# print(final_list)

	# li_container = films_container.ul.li.a

	# for movie in movie_container:

	# movies.append(str(movie_container.text))
	#wiki_movie_url = str(movie_container['href'])
	#print(type(base_url + wiki_movie_url))
	#movies_url.append[base_url + wiki_movie_url]

	# counter = 0
	# while(films_container.ul.li.a.find_next() is not None):
	# 	counter += 1
	# 	print(counter)




	# filmography_list = films_container.text.strip().split('\n')
	# print(filmography_list)

	# logging.info("Movie pages scraped: " + movie_page_scraped)
	# logging.info("Actor pages scraped: " + actor_page_scraped)
		
	# logging.info("Scraper ended")

if __name__ == "__main__":
	print(Scraper().scrape_page(None))