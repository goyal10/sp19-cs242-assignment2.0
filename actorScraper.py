from bs4 import BeautifulSoup
import requests
import logging


base_url = "https://en.wikipedia.org"

# parse infobox for actor
def parse_infobox(actor_infobox, actor_url, soup):
	actor_name = ""
	actor_age = 0
	actor_filmography = []

	# parsing actor name
	actor_name_infobox = actor_infobox.find("div", {"class": "fn"})
	if actor_name_infobox is None:
		logging.warning("Element not found: name")
	else:
		actor_name = actor_name_infobox.get_text()

	# parsing actor age
	actor_age_infobox = actor_infobox.find("span", {"class": "noprint ForceAgeToShow"})
	if actor_age_infobox is None:
		logging.warning("Element not found: age")
	else:
		actor_age = int(''.join(list(filter(str.isdigit, str(actor_age_infobox.contents[0])))))

	# parsing actor filmography
	actor_filmography_container = soup.find("span", {"id": "Filmography"})
	if actor_filmography_container is None:
		logging.warning("Element not found: filmography")
	else:
		actor_filmography = parse_filmography(actor_filmography_container)

	if actor_filmography is None:
		logging.error("Could not parse filmography")
		logging.warning("Scraping ending")
		return

	data_json = {
		"name": actor_name,
		"url": actor_url,
		"age": actor_age,
		"films": actor_filmography
	}

	return data_json

# scrape filmography
def parse_filmography(actor_filmography_container):
	filmography_list = []
	filmography_list_container = actor_filmography_container.find_next("ul")
	for film in filmography_list_container.findAll("i"):
		film_content = film.find("a")
		if film_content is None:
			return None
		film_url = base_url + film_content.get("href")
		film_name = film_content.get_text()
		filmography_list.append([film_name, film_url])

	return filmography_list

# scrape actor main method
def scrape_actor_page(actor_url):
	logging.basicConfig(filename="scraper.log")
	logging.info("Scraping actor url: " + actor_url)

	page = requests.get(actor_url)
	soup = BeautifulSoup(page.text, features="html.parser")
	soup.prettify()

	actor_infobox = soup.find("table", attrs={"class": "infobox biography vcard"})
	if actor_infobox is None:
		logging.error("Element not found: infobox")
		logging.warning("Scraping ending")
		return

	actor_data_json = parse_infobox(actor_infobox, actor_url, soup)
	return actor_data_json

# displays information. sanity check
def check_scraper():
	test_url = "https://en.wikipedia.org/wiki/Morgan_Freeman"
	# scrape_actor(test_url)
	print(scrape_actor_page(test_url))

# check_scraper()
