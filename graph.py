import json


# Class to create graph -> edges, nodes
class graph:
	edges = []
	nodes = []

	def __init__(self, filename):
		with open(filename) as filedata:
			self.data = json.load(filedata)
			filedata.close()
		

class node:
	name = ""
	age = 0
	earning = 0
	isActor = False
	isMovie = False
	edges = []


# Represents connection between nodes  in graph.
class edge:
	actor = None
	movie = None