from bs4 import BeautifulSoup
import requests
import logging
import re


base_url = "https://en.wikipedia.org"


# scrape movie main method
def scrape_movie_page(movie_url):
	logging.basicConfig(filename="scraper.log")
	logging.info("Scraping movie url: " + movie_url)

	page = requests.get(movie_url)
	soup = BeautifulSoup(page.text, features="html.parser")

	# parse infobox
	movie_infobox = soup.find("table", attrs={"class": "infobox vevent"})
	if movie_infobox is None:
		logging.error("Element not found: infobox")
		logging.warning("Scraping ending")
		return

	movie_data_json = parse_infobox(movie_infobox, movie_url)
	return movie_data_json


# parse infobox for movie
def parse_infobox(movie_infobox, movie_url):
	movie_name = ""
	movie_date = 0
	movie_cast = []
	movie_earning = 0

	# parsing movie name
	movie_name_infobox = movie_infobox.find("th", attrs={"class": "summary"})
	if movie_name_infobox is None:
		logging.warning("Element not found: name")
	else:
		movie_name = str(movie_name_infobox.text)

	# parsing movie date
	movie_date_infobox = movie_infobox.find("span", attrs={"class": "bday dtstart published updated"})
	if movie_date_infobox is None:
		logging.warning("Element not found: date")
	else:
		movie_date = int(movie_date_infobox.contents[0].split("-")[0])

	# parsing starring list
	movie_starring_infobox = movie_infobox.find(text=re.compile("Starring"))
	if movie_starring_infobox is None:
		logging.warning("Element not found: starring")
	else:
		movie_starring_infobox = movie_starring_infobox.find_next("ul")
		movie_cast = scrape_starring_infobox(movie_starring_infobox)


	# parsing earning
	movie_earning_infobox = movie_infobox.find(text=re.compile("Box office"))
	if movie_earning_infobox is None:
		logging.warning("Element not found: box office")
	else:
		movie_earning = convert_earning_to_int(movie_earning_infobox.find_next("td").contents[0])


	data_json = {
		"name": movie_name,
		"url": movie_url,
		"year": movie_date,
		"earning": movie_earning,
		"cast": movie_cast
	}

	return data_json

# scrape starring information from infobox
def scrape_starring_infobox(movie_starring_infobox):
	cast_count = 0
	cast = []
	for star in movie_starring_infobox.find_all("a"):
		cast_count += 1
		star_name = star.get_text()
		star_url = base_url + star.get("href")
		star_weight = cast_count
		cast.append([(star_name), (star_url), (star_weight)])

	return cast

# connvert earning value to appropriate integer
def convert_earning_to_int(earning_container):
	earning_string = str(earning_container)
	earning_string = earning_string.replace("$", "").replace("[", "").replace("]", "").replace(",", "")

	if " million" in earning_string:
		earning_string = earning_string.split(" ")
		earning_string[0] = float(earning_string[0])
		earning_string[0] *= 1000000

	elif " billion" in earning_string:
		earning_string = earning_string.split(" ")
		earning_string[0] = float(earning_string[0])
		earning_string[0] *= 1000000000

	return int(earning_string[0])

# displays information. sanity check
def check_scraper():
	test_url = "https://en.wikipedia.org/wiki/500_Days_of_Summer"
	print(scrape_movie_page(test_url))

# check_scraper()
